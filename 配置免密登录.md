# 部署机

tikv(200G+) * 3

```
# 磁盘挂载
fdisk -l
parted -s -a optimal /dev/sdb mklabel gpt -- mkpart primary ext4 1 -1
yes|mkfs.ext4 /dev/sdb
mkdir -p /data/
sudo cat >> /etc/fstab<<EOF
/dev/sdb /data/ ext4 defaults,nodelalloc,noatime 0 2
EOF
mount -a
mount -t ext4
df -h /data/
```



# 中控机

## root账号

```
# 准备环境
yum -y install epel-release git curl sshpass
yum -y install python2-pip
# 准备用户
useradd -m -d /home/tidb tidb
passwd tidb
# 修改免密(添加)
visudo

tidb ALL=(ALL) NOPASSWD: ALL
```

## tidb账号

方式一: 通过shell切换账号(有问题)

```
su - tidb
```

方拾二: 重新登录(推荐)





```
# 生成认证密钥
ssh-keygen -t rsa
```



```
# 拉取ansible脚本项目
export tag=v3.0.1
echo $tag
git clone -b $tag https://github.com/pingcap/tidb-ansible.git

# 安装依赖
cd /home/tidb/tidb-ansible
sudo pip install -r ./requirements.txt
ansible --version
```



```
# 配置主机信息(参考如下)
cd /home/tidb/tidb-ansible
vi hosts.ini
```



```
[servers]
172.16.10.1
172.16.10.2
172.16.10.3
172.16.10.4
172.16.10.5
172.16.10.6

[all:vars]
username = tidb
ntp_server = pool.ntp.org
```



```
# 运行互信脚本
ansible-playbook -i hosts.ini create_users.yml -u root -k

# 运行同步时区脚本
ansible-playbook -i hosts.ini deploy_ntp.yml -u tidb -b

# 运行cpu性能模式
ansible -i hosts.ini all -m shell -a "cpupower frequency-set --governor performance" -u tidb -b
```



```
# 修改机器资源分配(参考如下)
vi /home/tidb/tidb-ansible/inventory.ini
```



```
[tidb_servers]
172.16.10.1
172.16.10.2

[pd_servers]
172.16.10.1
172.16.10.2
172.16.10.3

[tikv_servers]
172.16.10.4
172.16.10.5
172.16.10.6

[monitoring_servers]
172.16.10.1

[grafana_servers]
172.16.10.1

[monitored_servers]
172.16.10.1
172.16.10.2
172.16.10.3
172.16.10.4
172.16.10.5
172.16.10.6
```



```
# 检验互信
ansible -i inventory.ini all -m shell -a 'whoami'
# 下载tidb部署二进制文件
ansible-playbook local_prepare.yml
# 初始化tidb,修改系统内核参数
ansible-playbook bootstrap.yml
# 部署
ansible-playbook deploy.yml
# 启动
ansible-playbook start.yml
```

