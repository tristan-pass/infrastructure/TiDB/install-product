# 搭建k8s集群

## 安装rancher

https://gitlab.com/tristan-pass/infrastructure/docker/install

https://gitlab.com/tristan-pass/infrastructure/rancher/install




# 初始化部署服务器

## 安装docker

https://gitlab.com/tristan-pass/infrastructure/docker/install

## 安装rancher-agent

```
docker stop $(docker ps -qa)
docker rm $(docker ps -qa)
docker ps -a

sudo docker run -d --privileged --restart=unless-stopped --net=host -v /etc/kubernetes:/etc/kubernetes -v /var/run:/var/run rancher/rancher-agent:v2.2.4 --server https://rancher.test.tidb.yibainetworklocal.com --token tvts6hnd9fmpwfp8dzdph5jb2ksm7zpptkkmhplqwjwdsxcw49hlmn --ca-checksum 39f1d6aeecfb13a4993d86d6f6486c63094517c69056a9f1f9857df2f3268947 --etcd --controlplane --worker
```



# 部署TiDB-k8s

## 前提要求

```
Kubernetes v1.10 +
DNS addons
PersistentVolume
RBAC enabled (optional)
Helm  >= v2.8.2 and < v3.0.0
Kubernetes v1.12 is required for zone-aware persistent volumes.
```

rancher-k8s的配置

```
# 修改镜像仓库地址为
registry.cn-shanghai.aliyuncs.com

# rancher工作节点修改/etc/hosts文件
echo `ifconfig -a|grep inet|grep -v 127.0.0.1|grep -v inet6 | awk '{print $2}' | tr -d "addr:"` >> /etc/hosts
# # 或者强制修改
echo "192.168.71.91 192.168.71.91" >> /etc/hosts
echo "192.168.71.92 192.168.71.92" >> /etc/hosts
echo "192.168.71.93 192.168.71.93" >> /etc/hosts
echo "192.168.71.94 192.168.71.94" >> /etc/hosts
echo "192.168.71.95 192.168.71.95" >> /etc/hosts
echo "192.168.71.96 192.168.71.96" >> /etc/hosts
```

安装kubectl

```
# 配置镜像仓库地址
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=http://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=http://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg http://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF

# 安装kubectl
yum install -y kubectl

# 配置集群密钥
mkdir -p ~/.kube
cat <<EOF > ~/.kube/config

EOF
```

密钥内容:

```
apiVersion: v1
kind: Config
clusters:
- name: "tidb"
  cluster:
    server: "https://rancher.test.tidb.yibainetworklocal.com/k8s/clusters/c-tkrgj"
    certificate-authority-data: "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUM3akNDQ\
      WRhZ0F3SUJBZ0lCQURBTkJna3Foa2lHOXcwQkFRc0ZBREFvTVJJd0VBWURWUVFLRXdsMGFHVXQKY\
      21GdVkyZ3hFakFRQmdOVkJBTVRDV05oZEhSc1pTMWpZVEFlRncweE9UQTJNamN3TmpFNU5EVmFGd\
      zB5T1RBMgpNalF3TmpFNU5EVmFNQ2d4RWpBUUJnTlZCQW9UQ1hSb1pTMXlZVzVqYURFU01CQUdBM\
      VVFQXhNSlkyRjBkR3hsCkxXTmhNSUlCSWpBTkJna3Foa2lHOXcwQkFRRUZBQU9DQVE4QU1JSUJDZ\
      0tDQVFFQXErandPdVhiWGZmRWE2RkoKV1FkMnFLWkdhMWFNYXBQRjU0WFhzSnJaOGtPckp6SjUyc\
      jNzb2VKOEIwaDM4b1lwOC81dUwrQXA3T3JwaW5mNwpnNHh4cUFhU1NsYzE5M3lFa0UzN2FDWG9za\
      FNkME1RQWlQRTVsRmM4My9MU3NsWlU2R3dwMWdPK0xFZzZXd21wCmRIdmsyMGQxaHpIUTVGRnNRZ\
      lNPaTRYVVBPeVAybG9WbWZnMnFrZWxwME45NHJWZXRLTnplQlU2Tk1vRWN0bVQKcTVMMUVVQlJRT\
      1VCRUwzOHBBMmYrRG5rYXluZ0JSRTU3V2VvMzc0eTIrNEUyZlNjSUVMcE9wNmdNc3E0K21QVQpCU\
      0JYN1RILzhNbmRHR3Q0ek5taWFSS1htM3BIbCtKc0Y5enJWZnp5aXd0NWZsMjFKZW80ZVpObzdRV\
      W1qMkJqCm5HQXI2UUlEQVFBQm95TXdJVEFPQmdOVkhROEJBZjhFQkFNQ0FxUXdEd1lEVlIwVEFRS\
      C9CQVV3QXdFQi96QU4KQmdrcWhraUc5dzBCQVFzRkFBT0NBUUVBWUJoejNpOXNCcXpKS2t4dXN6M\
      jhuQVJsTy9UK1BmK2oyMmFTazhvYQp6elFxMEZrVzBxT3ovVG5hb1RrY1JLMkhDR2owWkFFL0lla\
      ThEaHpNOGx5bStLclkvM0JZV1B3Yk8rNjU1QnY5CklZcTV3U1I3UGtLV0szYnQ4VlZ4VkxiSnhEU\
      21FYjF6M285Z2twdEJMbzJlWUtZVTdzSzdQOVVzL3puZENKcnEKS3ZqeFRkZHQ2V3A0WDU4V1dsa\
      3d3Nzhud1R2NXZtOGlEb3pYc1NFWXZPcmJ3YVpKUWVHd085VWpldmVUcXZTRgo3Uk5MYnZnNEk1T\
      05PMFFFTFJoMkl1UWhVYTVqVk9rbEtzS0ZmdS93TlZWejVIaGRlb1lOYlJaeTZBaklvTCtmClVyO\
      FV5RTJMVFpoR1hsSVRUTUJ1RmlrT2ttbHlab1I4aFZsUmc4MWtmUmxYZlE9PQotLS0tLUVORCBDR\
      VJUSUZJQ0FURS0tLS0t"
- name: "tidb-192"
  cluster:
    server: "https://192.168.71.91:6443"
    certificate-authority-data: "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUN3akNDQ\
      WFxZ0F3SUJBZ0lCQURBTkJna3Foa2lHOXcwQkFRc0ZBREFTTVJBd0RnWURWUVFERXdkcmRXSmwKT\
      FdOaE1CNFhEVEU1TURZeU56QTRNREkwTUZvWERUSTVNRFl5TkRBNE1ESTBNRm93RWpFUU1BNEdBM\
      VVFQXhNSAphM1ZpWlMxallUQ0NBU0l3RFFZSktvWklodmNOQVFFQkJRQURnZ0VQQURDQ0FRb0NnZ\
      0VCQU5WMmNKSFFJSHFSCmh0UzNHRmFVSkdvdHRPWklpcHJxMklrc2JkNUorV1FHVFpzUGxRQzY1e\
      TUyb3FnRjhqdnhiaXRMVWhvU0tIalAKZVYyeWt6TWIvQ21LbGt2ZnltQS9hU1dYeCswSkl2bnFxS\
      TdzZ3R1YVRFN0NoWU1uRUsveUdCNmYxcVBmOUJwcQpJWjRzY3NodTB6S3BDZ2x6TjlIZ1I1Rkhhc\
      FdvS0h3b09rNUcxWVpRc1FrM01NQWhwdy9oWndUWkVUVVhaZlNOClV5UDJpM002Z292R2JJeFMyc\
      zJjbTFNTWQ1RGtKdit3eDZCYjVoT1hmbW9qNUtLNjdHd2hESnM5VTc3L3orZmYKem1xY0dWZGFoV\
      URBbStHV2RlNHVZZ0dycVg4SjY5THA5aGtRRm9xaFJnYnBwK2ZNMDcrN1EyTUo5bGNYZU44agovL\
      01aNjdlcnA4a0NBd0VBQWFNak1DRXdEZ1lEVlIwUEFRSC9CQVFEQWdLa01BOEdBMVVkRXdFQi93U\
      UZNQU1CCkFmOHdEUVlKS29aSWh2Y05BUUVMQlFBRGdnRUJBQUp1Q2ZMSmhQQTFBdWFGL21zRERtN\
      ktLSHdnL1ZPemE3YzUKL1pNWXVkSlNwSXN3Ym1VTjdDOTRBVjhKbTV5ZlpHY3ZkZDdJaW9MdEJ6R\
      E1UVWlqQ2NXWVFjdmRSamt2alhwVApEclZSQTNrZE5ROE0yU0h4d2dDaGpUaWJyWnM5Q3NsQjRac\
      kNsbUFFR0NRRUN5VXlaUnRoUWVBR0kwRTIwMkNvCkVFd0podVdWOFRoMU9sR0tCdEpDUEM1QUhoQ\
      3RianZRQ0FXYVRBaHJvZmo1ZVZyaEdYQkc4VklSRVU5bmxkdnEKanlvUVJFNFllZkNkZm9CTU9OQ\
      jI3S1RFSTBCYXVQUnA0NUFvTnBraWFwajhrMVJnTWtERy83bklwRWVUT0JwYwo2dkZ5a05aa2hSY\
      mk5QSt4REprQyt1c2J5VU9Hc01GdGpWOFFYZmRmZHhJRStPeW5yZkU9Ci0tLS0tRU5EIENFUlRJR\
      klDQVRFLS0tLS0K"

users:
- name: "user-dq8qw"
  user:
    token: "kubeconfig-user-dq8qw.c-tkrgj:zkx79xmq4m76bv8mvf4sccs6vkcqpknrvvmfgmwfsvs6p8w4wkms5v"

contexts:
- name: "tidb"
  context:
    user: "user-dq8qw"
    cluster: "tidb"
- name: "tidb-192"
  context:
    user: "user-dq8qw"
    cluster: "tidb-192"

current-context: "tidb"

```





安装helm

```
# 安装helm
wget https://storage.googleapis.com/kubernetes-helm/helm-v2.11.0-linux-amd64.tar.gz
tar -zxvf helm-v2.11.0-linux-amd64.tar.gz
cd linux-amd64/
cp helm /usr/local/bin
```

清理

```
kubectl delete namespace pingcap
helm delete tidb-operator --purge
helm delete tidb-cluster  --purge
```

参考文档:

https://pingcap.com/docs-cn/v3.0/how-to/get-started/local-cluster/install-from-kubernetes-gke/

```
export chartVersion=v1.0.0-beta.3

# 添加helml仓库
helm repo add pingcap http://charts.pingcap.org/ && helm repo list
# 查看可用的 chart
helm repo update && helm search tidb-cluster -l && helm search tidb-operator -l
# 准备部署tidb集群
kubectl apply -f ./manifests/crd.yaml && \
kubectl apply -f ./manifests/gke/persistent-disk.yml && \
helm install pingcap/tidb-operator -n tidb-admin --namespace=tidb-admin --version=${chartVersion}
# 观察 Operator 启动情况
kubectl get pods --namespace tidb-admin -o wide --watch

# 部署tidb集群
helm install pingcap/tidb-cluster -n yibainetwork --namespace=tidb --set pd.storageClassName=pd-ssd,tikv.storageClassName=pd-ssd --version=${chartVersion}
# 观察tidb-cluster启动情况
kubectl get pods --namespace tidb -o wide --watch
# # 默认包含 2 个 TiDB pod，3 个 TiKV pod 和 3 个 PD pod

```



访问集群

```
kubectl get svc -n tidb --watch
```

维护集群

```
# 扩容
helm upgrade demo pingcap/tidb-cluster --set pd.storageClassName=pd-ssd,tikv.storageClassName=pd-ssd,tikv.replicas=5 --version=${chartVersion}

# 销毁
helm delete yibainetwork --purge
# 销毁数据存储
kubectl delete pvc -n tidb -l app.kubernetes.io/instance=demo,app.kubernetes.io/managed-by=tidb-operator && \
kubectl get pv -l app.kubernetes.io/namespace=tidb,app.kubernetes.io/managed-by=tidb-operator,app.kubernetes.io/instance=demo -o name | xargs -I {} kubectl patch {} -p '{"spec":{"persistentVolumeReclaimPolicy":"Delete"}}'
```







# 域名映射

mysql.test.tidb.yibainetworklocal.com

http://grafana.test.tidb.yibainetworklocal.com

# 参考文档

https://github.com/pingcap/tidb-operator/blob/master/docs/local-dind-tutorial.md

https://www.pingcap.com/blog-cn/tidb-tools-ecosystems/

https://zhuanlan.zhihu.com/newsql

